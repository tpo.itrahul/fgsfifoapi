﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Despatch_Request")]
    public class FGS_FIFO_Despatch_Request 

    {

        [Key]
        public int Desp_Req_Id { get; set; }
       
    [ForeignKey(nameof(FGS_FIFO_Material_Master))]
    [Required(ErrorMessage = "Material_Id is required")]
     public int Material_Id { get; set; }
     public virtual FGS_FIFO_Material_Master FGS_FIFO_Material_Master { get; set; }

    //[Required(ErrorMessage = "Material_Desc is required")]
    //[StringLength(100, ErrorMessage = "Material_Desc cannot be longer than 100 characters")]
    //public string Material_Desc { get; set; }
        
     [ForeignKey(nameof(FGS_FIFO_Customer_Master))]
     [Required(ErrorMessage = "Customer_id is required")]
    public int Customer_id { get; set; }

     [Required(ErrorMessage = "Quantity is required")]
     public int Quantity { get; set; }
   
    [Required(ErrorMessage = "Requested_DateTime is required")]
    public DateTime Requested_DateTime { get; set; }

   
   
    public string Entered_By { get; set; }
    

    [Required(ErrorMessage = "Invoice_No is required")]
    [StringLength(150, ErrorMessage = "Invoice_No cannot be longer than 150 characters")]
    public string Invoice_No { get; set; }

    [Required(ErrorMessage = "Invoice_Date is required")]
    public DateTime Invoice_Date { get; set; }
    
    public int Request_Status { get; set; }

    public ICollection<FGS_FIFO_Daily_Despatch_Transaction> FGS_FIFO_Daily_Despatch_Transaction { get; set; }

     public FGS_FIFO_Customer_Master FGS_FIFO_Customer_Master { get; set; }

    }
}



