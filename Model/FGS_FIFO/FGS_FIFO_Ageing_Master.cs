﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Ageing_Master")]
    public class FGS_FIFO_Ageing_Master

    {
        [Key]
        public int Ageing_Id { get; set; }


    //    public int? Material_Type_Id { get; set; }


    ////    [ForeignKey(nameof(FGS_FIFO_Material_Master))]
    // //   [Required(ErrorMessage = "Material_Id is required")]
    //    public int  Material_Id { get; set; }
    ////    public FGS_FIFO_Material_Master FGS_FIFO_Material_Master { get; set; }

        [Required(ErrorMessage = "Ageing_Months is required")]
        public int? Ageing_Months { get; set; }

        public int? Status { get; set; }
    }
}

