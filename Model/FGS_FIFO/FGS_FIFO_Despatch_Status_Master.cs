﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Despatch_Status_Master")]
    public class FGS_FIFO_Despatch_Status_Master

    {
        [Key]
        // public int Desp_Req_Id { get; set; }

        [Required(ErrorMessage = "Desp_Status_Id is required")]
      
        public int Desp_Status_Id { get; set; }
                           
        public string Desp_Status_Name { get; set; }
    }
}
