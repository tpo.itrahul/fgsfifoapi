﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.FGS_FIFO
{
    [Table("FGS_FIFO_Daily_Despatch_Transaction")]
    public class FGS_FIFO_Daily_Despatch_Transaction

    {

        [Key]
        public int Desp_TransId { get; set; }

        [ForeignKey(nameof(FGS_FIFO_Despatch_Request))]
        [Required(ErrorMessage = "Desp_Req_Id is required")]
        public int Desp_Req_Id { get; set; }
        public FGS_FIFO_Despatch_Request FGS_FIFO_Despatch_Request { get; set; }

        [Required(ErrorMessage = "Inv_TransId is required")]
        public int? Inv_TransId { get; set; }

        [Required(ErrorMessage = "Quantity_Despatched is required")]
        public int Quantity_Despatched { get; set; }
                          
        [Required(ErrorMessage = "EntryDate is required")]
        public DateTime EntryDate { get; set; }

        public string? Entered_By { get; set; }
       
        //public int Status { get; set; }

        [ForeignKey(nameof(FGS_FIFO_Despatch_Status_Master))]
        [Required(ErrorMessage = "Desp_Status_Id is required")]
        public int? Desp_Status_Id { get; set; }
        public FGS_FIFO_Despatch_Status_Master FGS_FIFO_Despatch_Status_Master { get; set; }

        [Required(ErrorMessage = "vehicle_number is required")]
        [StringLength(50, ErrorMessage = "vehicle_number cannot be longer than 50 characters")]
        public string? vehicle_number { get; set; }

        public string? Barcode { get; set; }

    }
}





