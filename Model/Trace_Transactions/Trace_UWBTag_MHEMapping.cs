﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model.Trace_Transactions
{
    [Table("Trace_UWBTag_MHEMapping")]
    public class Trace_UWBTag_MHEMapping
    {

        [Key]
        public int Tag_MHE_Id { get; set; }
        public int UWB_Tag_Id { get; set; }
        public int MHE_Id { get; set; }
        public DateTime Installed_Date { get; set; }
        public int Status { get; set; }

    }
}
