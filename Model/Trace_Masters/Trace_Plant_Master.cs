﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model.Trace_Masters
{
    [Table("Trace_Plant_Master")]
    public class Trace_Plant_Master
    {


        [Key]
        
        public int Plant_Id { get; set; }

        public string? Plant_Name { get; set; }
        public string? Plant_Address { get; set; }
         public int? Plant_Status { get; set; }

    }
}
