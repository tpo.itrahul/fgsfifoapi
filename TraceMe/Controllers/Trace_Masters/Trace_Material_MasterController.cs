﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;

namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Material_MasterController : ControllerBase
    {

        private readonly IRepositoryWrapper _repoWrapper;

        public Trace_Material_MasterController(IRepositoryWrapper repowrapper)
        {

            _repoWrapper = repowrapper;
        }

        // GET: api/Trace_Material_Master
        [HttpGet]
        public async Task<IActionResult> GetTrace_Material_Master()
        {
            try
            {
                var result = await _repoWrapper.IMaterialMaster.GetFGS__Material_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");

            }
        }
        // GET: api/Trace_Material_Master/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Material_Master(int id)
        {
            try
            {
                var trace_Material_Master = await _repoWrapper.IMaterialMaster.GetFGS__Material_Master((id));
                if (trace_Material_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_Material_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_Material_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Material_Master(int id, TraceMaterialMasterDTO trace_Material_Master)
        {
            if (id != trace_Material_Master.Material_Id)
            {
                return BadRequest();
            }



            try
            {
                if (trace_Material_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IMaterial_Master.GetFGS__Material_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IMaterialMaster.UpdateFGS__Material_Master(trace_Material_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Material_Master);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Material_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Material_Master([FromBody] TraceMaterialMasterDTO trace_Material_Master)
        {
            try
            {
                if (trace_Material_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IMaterialMaster.CreateFGS__Material_Master(trace_Material_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetFGS__Material_Master", new { id = trace_Material_Master.Material_Id }, trace_Material_Master);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // DELETE: api/Trace_Material_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Material_Master(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IMaterialMaster.GetFGS__Material_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IMaterialMaster.DeleteFGS__Material_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
    }
}