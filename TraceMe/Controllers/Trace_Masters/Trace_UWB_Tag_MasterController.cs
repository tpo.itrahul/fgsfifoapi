﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceMasters;
using Model.Trace_Masters;
using Repository;
namespace TraceMe.Controllers.Trace_Masters
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_UWB_Tag_MasterController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_UWB_Tag_MasterController(IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_UWB_Tag_Master
        [HttpGet]
        public async Task<IActionResult> GetTrace_UWB_Tag_Master()
        {
            try
            {
                var result = await _repoWrapper.IUWBTagMaster.GetTrace_UWB_Tag_Master();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // GET: api/Trace_UWB_Tag_Master/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_UWB_Tag_Master(int id)
        {

            try
            {
                var trace_UWB_Tag_Master = await _repoWrapper.IUWBTagMaster.GetTrace_UWB_Tag_Master((id));
                if (trace_UWB_Tag_Master == null)
                {
                    return NotFound();
                }
                return Ok(trace_UWB_Tag_Master);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // PUT: api/Trace_UWB_Tag_Master/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_UWB_Tag_Master(int id, TraceUWBTagMasterDTO trace_UWB_Tag_Master)
        {
            if (id != trace_UWB_Tag_Master.UWB_Tag_Id)
            {
                return BadRequest();
            }

            try
            {
                if (trace_UWB_Tag_Master == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IUWBTagMaster.GetTrace_UWB_Tag_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IUWBTagMaster.UpdateTrace_UWB_Tag_Master(trace_UWB_Tag_Master);
                await _repoWrapper.SaveAsync();
                return Ok(trace_UWB_Tag_Master);


            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_UWB_Tag_Master
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_UWB_Tag_Master(TraceUWBTagMasterDTO trace_UWB_Tag_Master)
        {
            try
            {
                if (trace_UWB_Tag_Master == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.IUWBTagMaster.CreateTrace_UWB_Tag_Master(trace_UWB_Tag_Master);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Material_Consumed_Spec", new { id = trace_UWB_Tag_Master.UWB_Tag_Id }, trace_UWB_Tag_Master);
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // DELETE: api/Trace_UWB_Tag_Master/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_UWB_Tag_Master(int id)
        {
            try
            {
                var ifexist = _repoWrapper.IUWBTagMaster.GetTrace_UWB_Tag_Master(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.IUWBTagMaster.DeleteTrace_UWB_Tag_Master(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }
    }
}
