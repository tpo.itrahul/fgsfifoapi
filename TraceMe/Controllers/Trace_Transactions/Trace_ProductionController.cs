﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceTransactions;
using Model.Trace_Transactions;
using Repository;
namespace TraceMe.Controllers.Trace_Transactions
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_ProductionController : ControllerBase
    {
        
        private IRepositoryWrapper _repoWrapper;
        public Trace_ProductionController( IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;
          
        }

        // GET: api/Trace_Production
        [HttpGet]
        public async Task<IActionResult> GetTrace_Production()
        {
            try
            {
                var result = await _repoWrapper.ITraceProduction.GetTrace_Production();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Production/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Production(int id)
        {

            try
            {
                var trace_Production = _repoWrapper.ITraceProduction.GetTrace_Production(id);
                if (trace_Production == null)
                {
                    return NotFound();
                }
                return Ok(trace_Production);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        
        }

        // PUT: api/Trace_Production/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Production(int id, TraceProductionDTO trace_Production)
        {
            if (id != trace_Production.Production_Id)
            {
                return BadRequest();
            }

          

            try
            {
                if (trace_Production == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ITakenToTB.GetTrace_Taken_ToTB(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ITraceProduction.UpdateTrace_Production(trace_Production);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Production);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Internal server error");
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");

            }
        }

            
        

        // POST: api/Trace_Production
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Production(TraceProductionDTO trace_Production)
        {
            try
            {
                if (trace_Production == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.ITraceProduction.CreateTrace_Production(trace_Production);
                await _repoWrapper.SaveAsync();
                return CreatedAtAction("GetTrace_Production", new { id = trace_Production.Production_Id }, trace_Production);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

         
        }

        // DELETE: api/Trace_Production/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Production(int id)
        {

            try
            {
                var ifexist = _repoWrapper.ITraceProduction.GetTrace_Production(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.ITraceProduction.DeleteTrace_Production(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }
       
        [HttpGet("{id}/{shift}")]
        public async Task<IActionResult> GetscheduleForDual_Shift(int id, string shift)
        {

            try
            {
                var ifexist =   _repoWrapper.ITraceProduction.GetscheduleForDual_Shift(id,shift);
                if (ifexist == null)
                {

                    return NotFound();
                }
               
                return Ok(ifexist);
             
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
