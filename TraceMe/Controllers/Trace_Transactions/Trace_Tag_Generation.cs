﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Entity.TraceTransactions;
using Model.Trace_Transactions;
using Repository;
namespace TraceMe.Controllers.Trace_Transactions
{
    [Route("api/[controller]")]
    [ApiController]
    public class Trace_Tag_GenerationController : ControllerBase
    {

        private IRepositoryWrapper _repoWrapper;
        public Trace_Tag_GenerationController(IRepositoryWrapper repowrapper)
        {
            _repoWrapper = repowrapper;

        }

        // GET: api/Trace_Tag_Generation
        [HttpGet]
        public async Task<IActionResult> GetTrace_Tag_Generation()
        {
            try
            {
                var result = await _repoWrapper.ITagGeneration.GetTrace_Tag_Generation();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

        // GET: api/Trace_Tag_Generation/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTrace_Tag_Generation(int id)
        {

            try
            {
                var trace_Tag_Generation = await _repoWrapper.ITagGeneration.GetTrace_Tag_Generation((id));
                if (trace_Tag_Generation == null)
                {
                    return NotFound();
                }
                return Ok(trace_Tag_Generation);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // PUT: api/Trace_Tag_Generation/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTrace_Tag_Generation(int id, TraceTagGenerationDTO trace_Tag_Generation)
        {
            if (id != trace_Tag_Generation.TagId)
            {
                return BadRequest();
            }


            try
            {
                if (trace_Tag_Generation == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.ITagGeneration.GetTrace_Tag_Generation(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.ITagGeneration.UpdateTrace_Tag_Generation(trace_Tag_Generation);
                await _repoWrapper.SaveAsync();
                return Ok(trace_Tag_Generation);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // POST: api/Trace_Tag_Generation
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<IActionResult> PostTrace_Tag_Generation(TraceTagGenerationDTO trace_Tag_Generation)
        {
            try
            {
                if (trace_Tag_Generation == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }
                _repoWrapper.ITagGeneration.CreateTrace_Tag_Generation(trace_Tag_Generation);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }

        // DELETE: api/Trace_Tag_Generation/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Tag_Generation(int id)
        {
            try
            {
                var ifexist = _repoWrapper.ITagGeneration.GetTrace_Tag_Generation(id);
                if (ifexist == null)
                {

                    return NotFound();
                }

                _repoWrapper.ITagGeneration.DeleteTrace_Tag_Generation(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        }

    }
}
