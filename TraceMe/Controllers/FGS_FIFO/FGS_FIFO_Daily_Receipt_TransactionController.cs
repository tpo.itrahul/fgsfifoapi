﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Entity.FGS_FIFO;
using Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using Entity.FGSFIFO;
using System.Threading.Tasks;

namespace TraceMe
{
    [Route("api/[controller]")]
    [ApiController]
    public class FGS_FIFO_Daily_Receipt_TransactionController : ControllerBase
    {
        private IRepositoryWrapper _repoWrapper;
        
        public FGS_FIFO_Daily_Receipt_TransactionController( IRepositoryWrapper repowrapper )
        {
             _repoWrapper = repowrapper;
            
        }


        //Modified get method using DTO, GetFGS_Daily_Receipt_Transaction() will return an object of "DailyReceiptTransactionDTO";
        [HttpGet]
        public async Task<IActionResult> GetFGS_Daily_Receipt_Transaction()
        {
            try
            {
                var result = await _repoWrapper.IDaily_ReceiptTranscation.GetFGS_Daily_Receipt_Transaction();
                //foreach(var item in result)
                //{
                //int SuppliedStock = _repoWrapper.IDaily_ReceiptTranscation.Get_Supplied_Stock(item.Material_Id);
                //int totdesptchreqMID = _repoWrapper.IDespatch_Request.TotQuantity_Despatch_Request_Placed_For_MID(item.Material_Id);
                // item.CurrentStock = SuppliedStock - totdesptchreqMID;
                // item.TotalConsumed = totdesptchreqMID;
                //}
                // available stock for scanned barcode
                //int currentstock = 0;
                foreach (var item in result)
                {
                    ///get total depatched here...reduce from supplied)
                    int stockinBarcode = _repoWrapper.IDaily_ReceiptTranscation.Get_Supplied_Stock_Barcode(item.Barcode);
                    int consumedstockBC = _repoWrapper.IDaily_DespatchTranscation.Get_tot_DespatchedForScannedBarcode(item.Barcode);
                    int AvailableStockInBarcode = stockinBarcode - consumedstockBC;
                    item.CurrentStock = AvailableStockInBarcode;
                    item.TotalConsumed = consumedstockBC;
                    item.Quantity = stockinBarcode;
                }

                // var result = await _repoWrapper.IDaily_ReceiptTranscation.GetFGS_Daily_Receipt_Transaction();
                return Ok(result);
            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

   

        }

        //Modified get method using DTO, GetFGS_Daily_Receipt_Transaction(int id) will return an object of "DailyReceiptTransactionDTO";
        [HttpGet("{id}")]
        public async Task<IActionResult> GetDaily_ReceiptTranscation(int id)
        {
            try
            {
                var fGS_FIFO_Daily_ReceiptTranscation = await _repoWrapper.IDaily_ReceiptTranscation.GetFGS_Daily_Receipt_Transaction((id));
                if (fGS_FIFO_Daily_ReceiptTranscation == null)
                {
                    return NotFound();
                }
                return Ok(fGS_FIFO_Daily_ReceiptTranscation);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }
        
           
        }
        [HttpGet("GetFGS_Daily_Receipt_Transaction_Barcode/{barcode}")]
          public async Task<IActionResult> GetFGS_Daily_Receipt_Transaction_Barcode(string barcode)
        {
            try
            {
                var fGS_FIFO_Daily_ReceiptTranscation = await _repoWrapper.IDaily_ReceiptTranscation.GetFGS_Daily_Receipt_Transaction_Barcode(barcode);
                if (fGS_FIFO_Daily_ReceiptTranscation.Count() == 0)
                {
                    return StatusCode(250, "BARCODENOTFOUND");
                }
               

                // available stock for scanned barcode
                //int currentstock = 0;
                foreach (var item in fGS_FIFO_Daily_ReceiptTranscation)
                {
                    ///get total depatched here...reduce from supplied)
                    int stockinBarcode = _repoWrapper.IDaily_ReceiptTranscation.Get_Supplied_Stock_Barcode(barcode);
                    int consumedstockBC = _repoWrapper.IDaily_DespatchTranscation.Get_tot_DespatchedForScannedBarcode(barcode);
                    int AvailableStockInBarcode = stockinBarcode - consumedstockBC;
                    item.CurrentStock = AvailableStockInBarcode;

                }
                return Ok(fGS_FIFO_Daily_ReceiptTranscation);

            }
            catch (Exception ex)
            {
                //_logger.LogError($"Something went wrong inside GetAllOwners action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }
        //Modified put method using DTO, PutFGS_FIFO_Daily_Receipt_Transaction() will accept an object of "DailyReceiptTransactionDTO and int id for confirmation of id to be updated";
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFGS_FIFO_Daily_Receipt_Transaction(int id, DailyReceiptTransactionDTO fGS_FIFO_Daily_ReceiptTranscation)
        {
            if (id != fGS_FIFO_Daily_ReceiptTranscation.Inv_TransId)
            {
                return BadRequest();
            }


            try
            {

                if (fGS_FIFO_Daily_ReceiptTranscation == null)

                {
                    return BadRequest("Object is null");
                }
                var ifexist = _repoWrapper.IDaily_ReceiptTranscation.GetFGS_Daily_Receipt_Transaction(id);
                if (ifexist == null)
                {

                    return NotFound();
                }
                _repoWrapper.IDaily_ReceiptTranscation.UpdateFGS_Daily_Receipt_Transaction(fGS_FIFO_Daily_ReceiptTranscation);
                await _repoWrapper.SaveAsync();
                return Ok(fGS_FIFO_Daily_ReceiptTranscation);

            }
            catch (DbUpdateConcurrencyException)
            {

                throw;

            }
          
        }

        //Modified post method using DTO, PostDaily_ReceiptTranscation() will accept an object of "DailyReceiptTransactionDTO";
        [HttpPost]
        public async Task<IActionResult> PostDaily_ReceiptTranscation([FromBody] DailyReceiptTransactionDTO model)
        {
            try
            {
                if (model == null)
                {
                    // _logger.LogError("Owner object sent from client is null.");
                    return BadRequest(" object is null");
                }



                _repoWrapper.IDaily_ReceiptTranscation.CreateFGS_Daily_Receipt_Transaction(model);
                await _repoWrapper.SaveAsync();
               // return Ok("Succees");

                return CreatedAtAction("GetFGS_Daily_Receipt_Transaction", new { id = model.Inv_TransId }, model);
            }
            catch (Exception ex)
            {
                // _logger.LogError($"Something went wrong inside CreateOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }


        }

        // DELETE: api/Trace_Material_Consumed_Spec/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTrace_Daily_Receipt_Transaction(int id)
        {
            try
            {
                //var ifexist = _repoWrapper.IDaily_ReceiptTranscation.GetFGS_Daily_Receipt_Transaction(id);
                //if (ifexist == null)
                //{

                //    return NotFound();
                //}

                _repoWrapper.IDaily_ReceiptTranscation.DeleteFGS_Daily_Receipt_Transaction(id);
                await _repoWrapper.SaveAsync();
                return Ok("Succees");
            }

            catch (Exception ex)
            {
                //  _logger.LogError($"Something went wrong inside DeleteOwner action: {ex.Message}");
                return StatusCode(500, "Internal server error");
            }

        }
     
    }
}
