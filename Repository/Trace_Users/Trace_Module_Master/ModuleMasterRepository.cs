﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.FGS_FIFO;
using Entity.TraceUsers;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Users
{
    public class ModuleMasterRepository : RepositoryBase<Trace_Module_Master>, IModuleMasterRespository
    {
        public ModuleMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<Trace_Module_MasterDTO>> GetTrace_Module_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new Trace_Module_MasterDTO()
            {
                Id = x.Id,
                Application_Id = x.Application_Id,
                Module_Id = x.Module_Id,
                Module_Name = x.Module_Name,
                Module_Status = x.Module_Status,
              
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<Trace_Module_MasterDTO>> GetTrace_Module_Master(int id)
        {


            var rows = FindByCondition(x => x.Id.Equals(id));
            var results = rows.Select(x => new Trace_Module_MasterDTO()
            {
                Id = x.Id,
                Application_Id = x.Application_Id,
                Module_Id = x.Module_Id,
                Module_Name = x.Module_Name,
                Module_Status = x.Module_Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Module_Master(Trace_Module_MasterDTO model)
        {
            var entity = new Trace_Module_Master();
            entity.Id = model.Id;
            entity.Application_Id = model.Application_Id;
            entity.Module_Id = model.Module_Id;
            entity.Module_Name = model.Module_Name;
            entity.Module_Status = model.Module_Status;
          

            Create(entity);
        }
        public void UpdateTrace_Module_Master(Trace_Module_MasterDTO model)
        {
            var entity = new Trace_Module_Master();
            if (model != null)
            {
                if (model.Id > 0)
                {
                    var data = FindByCondition(x => x.Id == model.Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
               
                entity.Id = model.Id;
                entity.Application_Id = model.Application_Id;
                entity.Module_Id = model.Module_Id;
                entity.Module_Name = model.Module_Name;
                entity.Module_Status = model.Module_Status;
            }
            Update(entity);
        }
        public void DeleteTrace_Module_Master(int id)
        {
            var entity = new Trace_Module_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Id = 0;
            }

            Update(entity);
        }
    }
}
