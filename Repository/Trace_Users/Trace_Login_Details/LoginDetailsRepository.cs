﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.FGS_FIFO;
using Entity.TraceUsers;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Users
{
    public class LoginDetailsRepository : RepositoryBase<Trace_Login_Details>, ILoginDetailsRepository
    {
        public LoginDetailsRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<Trace_Login_DetailsDTO>> GetTrace_Login_Details()
        {

            var rows = FindAll();
            var results = rows.Select(x => new Trace_Login_DetailsDTO()
            {
                Id = x.Id,
                Emp_Id = x.Emp_Id,
                Logged_Time = x.Logged_Time,
                Module_Id = x.Module_Id,
                Plant_Id = x.Plant_Id

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<Trace_Login_DetailsDTO>> GetTrace_Login_Details(int id)
        {


            var rows = FindByCondition(x => x.Id.Equals(id));
            var results = rows.Select(x => new Trace_Login_DetailsDTO()
            {
                Id = x.Id,
                Emp_Id = x.Emp_Id,
                Logged_Time = x.Logged_Time,
                Module_Id = x.Module_Id,
                Plant_Id = x.Plant_Id
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Login_Details(Trace_Login_DetailsDTO model)
        {
            var entity = new Trace_Login_Details();
            entity.Id = model.Id;
            entity.Emp_Id = model.Emp_Id;
            entity.Logged_Time = model.Logged_Time;
           entity.Module_Id = model.Module_Id;
            entity.Plant_Id = model.Plant_Id;


            Create(entity);
        }
        public void UpdateTrace_Login_Details(Trace_Login_DetailsDTO model)
        {
            var entity = new Trace_Login_Details();
            if (model != null)
            {
                if (model.Id > 0)
                {
                    var data = FindByCondition(x => x.Id == model.Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Id = model.Id;
                entity.Emp_Id = model.Emp_Id;
                entity.Emp_Id = model.Emp_Id;
                entity.Logged_Time = model.Logged_Time;
                entity.Module_Id = model.Module_Id;
                entity.Plant_Id = model.Plant_Id;
            }
            Update(entity);
        }
        public void DeleteTrace_Login_Details(int id)
        {
            var entity = new Trace_Login_Details();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Id = 0;
            }

            Update(entity);
        }
    }
}
