﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.TraceUsers;
using Entity.TraceUsers;
namespace Repository.Trace_Users
{
    public interface IApplicationEmployeeMappingRepository //: IRepositoryBase<Trace_Barcode_Generation>
    {
         Task<IEnumerable<Trace_Application_Employee_MappingDTO>> GetTrace_Application_Employee_Mapping();

        Task<IEnumerable<Trace_Application_Employee_MappingDTO>> GetTrace_Application_Employee_Mapping(int id);

       void CreateTrace_Application_Employee_Mapping(Trace_Application_Employee_MappingDTO model);

       void UpdateTrace_Application_Employee_Mapping(Trace_Application_Employee_MappingDTO model);

       void DeleteTrace_Application_Employee_Mapping(int id);
       


    }
}
