﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSDailyDespatchTransactionRepository
        //: IRepositoryBase<FGS_FIFO_Daily_Despatch_Transaction>
    {
        Task<IEnumerable<DailyDespatchTransactionDTO>> GetFGS_Daily_Despatch_Transaction();
        int CreateFGS_Daily_Despatch_Transaction(DailyDespatchTransactionDTO model);
        void UpdateFGS_Daily_Despatch_Transaction(DailyDespatchTransactionDTO model);
        void DeleteFGS_Daily_Despatch_Transaction(int id);
        Task<IEnumerable<DailyDespatchTransactionDTO>> GetFGS_Daily_Despatch_Transaction(int id);
        int Get_tot_despatchedForRequestId(int id);

        void ChangeTranscationProcessStatus(int tid, int status);

        int Get_tot_DespatchedForScannedBarcode(string id);


    }
}
