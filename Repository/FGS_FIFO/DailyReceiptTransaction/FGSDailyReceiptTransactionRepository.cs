﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Microsoft.EntityFrameworkCore;
using Entity.FGSFIFO;
using System.Text;
namespace Repository.FGS_FIFO
{
 //Service 
    public class FGSDailyReceiptTransactionRepository : RepositoryBase<FGS_FIFO_Daily_Receipt_Transaction>, IFGSDailyReceiptTransactionRepository
    {
        public FGSDailyReceiptTransactionRepository(Trace_DBContext repositoryContext) : base(repositoryContext)
        {
            
        }
        public async Task<IEnumerable<DailyReceiptTransactionDTO>> GetFGS_Daily_Receipt_Transaction()
        {

            var rows =  FindAll()
                 .Include(sid => sid.FGS_FIFO_Supplier_Master)
                 .Include(mid => mid.FGS_FIFO_Material_Master)
                 .Include(bid => bid.FGS_FIFO_Batch_Type_Master)
                 .Include(lid => lid.FGS_FIFO_Location_Master)
                 ;
            var results = rows.Select(x => new DailyReceiptTransactionDTO()
            {
                Entered_By = x.Entered_By,
                Suppliername = x.FGS_FIFO_Supplier_Master.Supplier_Name,
                Invoice_No = x.Invoice_No,
                Invoice_Date = x.Invoice_Date.ToShortDateString(),
                LocationName = x.FGS_FIFO_Location_Master.Location_Name,
                EntryDate = x.EntryDate.ToShortDateString(),
                BatchTypeName = x.FGS_FIFO_Batch_Type_Master.Batch_Type_Name,
                Barcode = x.Barcode,
                Exp_Date = x.Exp_Date.ToShortDateString(),
                MaterialCode = x.FGS_FIFO_Material_Master.Material_Code,
                MaterialDesc = x.FGS_FIFO_Material_Master.Material_Description,
                Material_Id = x.Material_Id,
                Status = x.Status,
                Quantity = x.Quantity,
                Inv_TransId = x.Inv_TransId,
                ProdDate = x.ProdDate.ToShortDateString(),

            }).ToListAsync();
            return await results;

        }
        public async Task<IEnumerable<DailyReceiptTransactionDTO>> GetFGS_Daily_Receipt_Transaction(int id)
        {
            var rows = FindByCondition(x => x.Inv_TransId.Equals(id))
                .Include(sid => sid.FGS_FIFO_Supplier_Master)
                 .Include(mid => mid.FGS_FIFO_Material_Master)
                 .Include(bid => bid.FGS_FIFO_Batch_Type_Master)
                 .Include(lid => lid.FGS_FIFO_Location_Master)
                 .OrderBy(edate => edate.EntryDate);

            var results = rows.Select(x => new DailyReceiptTransactionDTO()
            {
                Entered_By = x.Entered_By,
                Suppliername = x.FGS_FIFO_Supplier_Master.Supplier_Name,
                Invoice_No = x.Invoice_No,
                Invoice_Date = x.Invoice_Date.ToShortDateString(),
                LocationName = x.FGS_FIFO_Location_Master.Location_Name,
                EntryDate = x.EntryDate.ToShortDateString(),
                BatchTypeName = x.FGS_FIFO_Batch_Type_Master.Batch_Type_Name,
                Barcode = x.Barcode,
                Exp_Date = x.Exp_Date.ToShortDateString(),
                MaterialCode = x.FGS_FIFO_Material_Master.Material_Code,
                MaterialDesc = x.FGS_FIFO_Material_Master.Material_Description,
                Material_Id = x.Material_Id,
                Status = x.Status,
                Quantity = x.Quantity,
                Inv_TransId = x.Inv_TransId,
                ProdDate = x.ProdDate.ToShortDateString(),

            }).ToListAsync();
            return await results;

        }
        public async Task<IEnumerable<DailyReceiptTransactionDTO>> GetFGS_Daily_Receipt_Transaction_Barcode(string barcode)
        {
      //  barcode: "",materialDesc: "",quantity: 0,location_Id: 0,prodDate: "",exp_Date: "",invoice_No: "",entryDate: "",prodDate: ""
            var rows = FindByCondition(x => x.Barcode.Equals(barcode))
            .Include(sid => sid.FGS_FIFO_Supplier_Master)
             .Include(mid => mid.FGS_FIFO_Material_Master)
            .Include(bid => bid.FGS_FIFO_Batch_Type_Master)
             .Include(lid => lid.FGS_FIFO_Location_Master)
              .OrderBy(edate => edate.EntryDate);

            var results = rows.Select(x => new DailyReceiptTransactionDTO()
            {
               // Entered_By = x.Entered_By,
                // Suppliername = x.FGS_FIFO_Supplier_Master.Supplier_Name,
                Invoice_No = x.Invoice_No,
                Invoice_Date = x.Invoice_Date.ToShortDateString(),
                Location_Id = x.Location_Id,
                EntryDate = x.EntryDate.ToShortDateString(),
                //   BatchTypeName = x.FGS_FIFO_Batch_Type_Master.Batch_Type_Name,
                Barcode = x.Barcode,
                Exp_Date = x.Exp_Date.ToShortDateString(),
                //  MaterialCode = x.FGS_FIFO_Material_Master.Material_Code,
                MaterialDesc = x.FGS_FIFO_Material_Master.Material_Description,
                Material_Id = x.Material_Id,
                Status = x.Status,
                Quantity = x.Quantity,
                Inv_TransId = x.Inv_TransId,
                ProdDate = x.ProdDate.ToShortDateString(),

            }).ToListAsync();
            return await results;

        }
        public int CreateFGS_Daily_Receipt_Transaction (DailyReceiptTransactionDTO model)
        {
            var entity = new FGS_FIFO_Daily_Receipt_Transaction();
            if (model != null)
            {

                //if (model.Inv_TransId > 0)
                //{
                //    var data = FindByCondition(x => x.Inv_TransId == model.Inv_TransId);
                //    if (data.Any()) { entity = data.FirstOrDefault(); }

                //}
               // if (model.Inv_TransId <= 0)
               // { 
                    entity.Inv_TransId = model.Inv_TransId; 
               // }

                //if (model.Status != 0)
                //{
                    entity.Status = model.Status;
                //}

                //  if (!string.IsNullOrEmpty(model.Barcode))
                //{
                entity.Barcode = GenerateBarCode(model.Supplier_Id, model.Batch_Type_Id, model.Material_Id,model.Location_Id);
            //}

               // if (model.Batch_Type_Id != 0)
                //{
                    entity.Batch_Type_Id = model.Batch_Type_Id; 
            //}

               // if (!string.IsNullOrEmpty(model.Entered_By))
               // { 
                    entity.Entered_By = model.Entered_By;
            //}

               // if (model.EntryDate != DateTime.MinValue && model.EntryDate != DateTime.MaxValue)
                //{ 
                    entity.EntryDate =Convert.ToDateTime( model.EntryDate); 
            //}

               // if (model.Invoice_Date != DateTime.MinValue && model.Invoice_Date != DateTime.MaxValue)
                //{
                    entity.Invoice_Date = Convert.ToDateTime(model.Invoice_Date);
            //}

              //  if (model.Exp_Date != DateTime.MinValue && model.Exp_Date != DateTime.MaxValue)
                //{ 
                    entity.Exp_Date = Convert.ToDateTime(model.Exp_Date);
            //}

             //   if (model.Location_Id != 0)
              //  { 
                    entity.Location_Id = model.Location_Id;
            //}

              //  if (model.Material_Id != 0)
               // {
                    entity.Material_Id = model.Material_Id;
            //}

              //  if (!string.IsNullOrEmpty(model.Invoice_No))
             //   { 
                    entity.Invoice_No = model.Invoice_No; 
            //}

              //  if (model.ProdDate != DateTime.MinValue && model.ProdDate != DateTime.MaxValue)
               // { 
                    entity.ProdDate = Convert.ToDateTime(model.ProdDate); 
            //}

              //  if (model.Quantity != 0)
             //   { 
                    entity.Quantity = model.Quantity;
                //}
                entity.Supplier_Id = model.Supplier_Id;
                entity.Invoice_No = model.Invoice_No;
            }

            Create(entity);
            return entity.Inv_TransId;
        }
        public void UpdateFGS_Daily_Receipt_Transaction(DailyReceiptTransactionDTO model)
        {
            var entity = new FGS_FIFO_Daily_Receipt_Transaction();
            if (model != null)
            {

                if (model.Inv_TransId > 0)
                {
                    var data = FindByCondition(x => x.Inv_TransId == model.Inv_TransId);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                if (model.Inv_TransId > 0)
                 { entity.Inv_TransId = model.Inv_TransId; }
               
                if (model.Status != 0) 
                 { entity.Status = model.Status; }
                
                if (!string.IsNullOrEmpty(model.Barcode))
                 { entity.Barcode = model.Barcode; }
               
                if (model.Batch_Type_Id != 0)
                { entity.Batch_Type_Id = model.Batch_Type_Id; }

                if (!string.IsNullOrEmpty(model.Entered_By))
                { entity.Entered_By = model.Entered_By; }

                if (Convert.ToDateTime(model.EntryDate) !=  DateTime.MinValue && Convert.ToDateTime(model.EntryDate) != DateTime.MaxValue)
                { entity.EntryDate = Convert.ToDateTime(model.EntryDate); }

                if (Convert.ToDateTime(model.Invoice_Date) != DateTime.MinValue && Convert.ToDateTime(model.Invoice_Date) != DateTime.MaxValue)
                { entity.Invoice_Date = Convert.ToDateTime(model.Invoice_Date); }

                if (Convert.ToDateTime(model.Exp_Date) != DateTime.MinValue && Convert.ToDateTime(model.Exp_Date) != DateTime.MaxValue)
                { entity.Exp_Date = Convert.ToDateTime(model.Exp_Date); }

                if (model.Location_Id != 0)
                { entity.Location_Id = model.Location_Id; }

                if (model.Material_Id != 0)
                { entity.Material_Id = model.Material_Id; }

                if (!string.IsNullOrEmpty(model.Invoice_No))
                { entity.Invoice_No = model.Invoice_No; }

                if (Convert.ToDateTime(model.ProdDate) != DateTime.MinValue && Convert.ToDateTime(model.ProdDate) != DateTime.MaxValue)
                { entity.ProdDate = Convert.ToDateTime(model.ProdDate); }
               
                if (model.Quantity != 0)
                {  entity.Quantity = model.Quantity;}
                   

            }

            Update(entity);
        }
        public void DeleteFGS_Daily_Receipt_Transaction(int id)
        {
            var entity = new FGS_FIFO_Daily_Receipt_Transaction();
            if (id!= 0)
               
            {
                    var data = FindByCondition(x => x.Inv_TransId == id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }
                    //updated entity status to 0 after finding the invoice id
                    entity.Status = 0;
            }

            Update(entity);
             //Save();

        }
        public int Get_Supplied_Stock (int id)
        {
            int IncomingStock = 0;

            var result = FindByCondition(x => x.Material_Id == id);
            if (result.Any())
            {
                foreach (var stock in result)
                {
                    IncomingStock += stock.Quantity;

                }

            }
            return IncomingStock;
        }
        public int Get_Supplied_Stock_Barcode(string bc)
        {
            int Stock = 0;

            var result = FindByCondition(x => x.Barcode == bc);
            if (result.Any())
            {
                foreach (var stock in result)
                {
                    Stock += stock.Quantity;

                }

            }
            return Stock;
        }
        public void ChangeTranscationStatus(int tid, int status)
        {
            var entity = new FGS_FIFO_Daily_Receipt_Transaction();
            var data = FindByCondition(x => x.Inv_TransId == tid);
            if (data.Any())
            { 
                
                entity = data.FirstOrDefault();
                entity.Status = status;

            }
            Update(entity);
        }

        public string GenerateBarCode(int supplierid,int batchtypeid, int materialid, int loc)
        {
            Random _random = new Random();
            int genRand = _random.Next(10, 99);
            string Barcode = "FGS" + genRand;
            Barcode += Convert.ToString(supplierid);
            Barcode += Convert.ToString(batchtypeid);
            Barcode += Convert.ToString(materialid);
            Barcode += Convert.ToString(loc);
            return Barcode;
        }
        //
        public string GetLocationsMatID(int materialid)
        {
            string Locations = "";
            var entity = new FGS_FIFO_Daily_Receipt_Transaction();
            var data = FindByCondition(x => x.Material_Id == materialid);
            if (data.Any())
            {

                foreach(var item in data)
                {
                    Locations += item.Location_Id + ",";
                }

            }
            return Locations;
        }

        public string GetBarcodeforMatID(int materialid)
        {
            string Barcode = "";
            var entity = new FGS_FIFO_Daily_Receipt_Transaction();
            var data = FindByCondition(x => x.Material_Id == materialid);
            if (data.Any())
            {

                foreach (var item in data)
                {
                    Barcode += item.Barcode + ",";
                }

            }
            return Barcode;
        }

        //public string GetMaterialAge()


    }
}
