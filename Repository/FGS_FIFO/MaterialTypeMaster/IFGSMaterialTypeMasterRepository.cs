﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entity.FGSFIFO;
namespace Repository.FGS_FIFO
{
    public interface IFGSMaterialTypeMasterRepository 
        //: IRepositoryBase<FGS_FIFO_Material_Type_Master>
    {
       Task<IEnumerable<MaterialTypeMasterDTO>> GetFGS__Material_Master();

       Task<IEnumerable<MaterialTypeMasterDTO>> GetFGS__Material_Master(int id);

       void CreateFGS__Material_Master(MaterialTypeMasterDTO model);

       void UpdateFGS__Material_Master(MaterialTypeMasterDTO model);

      void DeleteFGS__Material_Master(int id);
       


    }
}
