﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;
using Entity.FGS_FIFO;
using Entity.FGSFIFO;
using Microsoft.EntityFrameworkCore;

namespace Repository.FGS_FIFO
{
    public class FGSAgeingMasterRepository : RepositoryBase<FGS_FIFO_Ageing_Master>, IFGSAgeingMasterRepository
    {
        public FGSAgeingMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public  async Task<IEnumerable<AgeingMasterDTO> > GetFGS_Ageing()
        {

            var rows = FindAll();
             //   .Include(x => x.FGS_FIFO_Material_Master);
            var results = rows.Select(x => new AgeingMasterDTO()
            {
                Ageing_Id = x.Ageing_Id,
               // Material_Id = x.Material_Id,
                Ageing_Months = x.Ageing_Months,
               // Material_Type_Id = x.Material_Type_Id,
                Status = x.Status,
               // Material_Desc = x.FGS_FIFO_Material_Master.Material_Description,
                
           
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<AgeingMasterDTO>> GetFGS_Ageing(int id)
        {


            var rows = FindByCondition(x => x.Ageing_Id.Equals(id));
            //.Include(x => x.FGS_FIFO_Material_Master);
            var results = rows.Select(x => new AgeingMasterDTO()
            {
                Ageing_Id = x.Ageing_Id,
               // Material_Id = x.Material_Id,
                Ageing_Months = x.Ageing_Months,
              //  Material_Type_Id = x.Material_Type_Id,
                Status = x.Status,
               // Material_Desc = x.FGS_FIFO_Material_Master.Material_Description

            }).ToListAsync();

            return await results;


        }
        public void CreateFGS_FGS_Ageing(AgeingMasterDTO model)
        {
            var ifpresent = FindByCondition(x => x.Ageing_Months == model.Ageing_Months);
            if(ifpresent!= null)
                {
                var entity = new FGS_FIFO_Ageing_Master();
                entity.Ageing_Id = model.Ageing_Id;
                entity.Ageing_Months = model.Ageing_Months;
               // entity.Material_Id = model.Material_Id;
               // entity.Material_Type_Id = model.Material_Type_Id;
                entity.Status = model.Status;

                Create(entity);
            }
        }
        public void UpdateFGS_Ageing(AgeingMasterDTO model)
        {
            var entity = new FGS_FIFO_Ageing_Master();
            if (model != null)
            {
                if (model.Ageing_Id > 0)
                {
                    var data = FindByCondition(x => x.Ageing_Id == model.Ageing_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Ageing_Id = model.Ageing_Id;
                entity.Ageing_Months = model.Ageing_Months;
               // entity.Material_Id = model.Material_Id;
               // entity.Material_Type_Id = model.Material_Type_Id;
                entity.Status = model.Status;
            }   
            Update(entity);
        }
        public void DeleteFGS_FGS_Ageing(int id)
        {
            var entity = new FGS_FIFO_Ageing_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Ageing_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Status = 0;
            }

            Update(entity);
        }

    }
}
