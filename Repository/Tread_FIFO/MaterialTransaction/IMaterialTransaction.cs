﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IMaterialTransactionRepository : IRepositoryBase<Trace_Material_Transaction>
    {
        Task<IEnumerable<TraceMaterialTransactionDTO>> GetTrace_Material_Transaction();

        Task<IEnumerable<TraceMaterialTransactionDTO>> GetTrace_Material_Transaction(int id);

        void CreateTrace_Material_Transaction(TraceMaterialTransactionDTO model);

        void UpdateTrace_Material_Transaction(TraceMaterialTransactionDTO model);

        void DeleteTrace_Material_Transaction(int id);
        
    }
}
