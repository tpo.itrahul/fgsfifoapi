﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.FGS_FIFO;
using Entity.TreadFIFO;
using Microsoft.EntityFrameworkCore;
namespace Repository.Tread_FIFO
{
    public class ScrapEntryRepository : RepositoryBase<Trace_Scrap_Entry>, IScrapEntryRepository
    {
        public ScrapEntryRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {

        }
        public async Task<IEnumerable<TraceScrapEntryDTO>> GetTrace_Scrap_Entry()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceScrapEntryDTO()
            {
                Scrap_EntryId = x.Scrap_EntryId,
                Material_Id = x.Material_Id,
                Entry_Date = x.Entry_Date,
                Entry_Time = x.Entry_Time,


            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceScrapEntryDTO>> GetTrace_Scrap_Entry(int id)
        {


            var rows = FindByCondition(x => x.Scrap_EntryId.Equals(id));
            var results = rows.Select(x => new TraceScrapEntryDTO()
            {
                Scrap_EntryId = x.Scrap_EntryId,
                Material_Id = x.Material_Id,
                Entry_Date = x.Entry_Date,
                Entry_Time = x.Entry_Time,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Scrap_Entry(TraceScrapEntryDTO model)
        {
            var entity = new Trace_Scrap_Entry();
            entity.Scrap_EntryId = model.Scrap_EntryId;
            entity.Material_Id = model.Material_Id;
            entity.Entry_Date = model.Entry_Date;
            entity.Entry_Time = model.Entry_Time;



            Create(entity);
        }
        public void UpdateTrace_Scrap_Entry(TraceScrapEntryDTO model)
        {
            var entity = new Trace_Scrap_Entry();
            if (model != null)
            {
                if (model.Scrap_EntryId > 0)
                {
                    var data = FindByCondition(x => x.Scrap_EntryId == model.Scrap_EntryId);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Scrap_EntryId = model.Scrap_EntryId;
                entity.Material_Id = model.Material_Id;
                entity.Entry_Date = model.Entry_Date;
                entity.Entry_Time = model.Entry_Time;
            }
            Update(entity);
        }
        public void DeleteTrace_Scrap_Entry(int id)
        {
            var entity = new Trace_Scrap_Entry();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Scrap_EntryId == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.Scrap_EntryId = 0;
            }

            Update(entity);
        }
    }
}
