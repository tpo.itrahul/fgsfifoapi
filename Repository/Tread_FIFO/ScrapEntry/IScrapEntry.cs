﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IScrapEntryRepository// : IRepositoryBase<Trace_Scrap_Entry>
    {
        Task<IEnumerable<TraceScrapEntryDTO>> GetTrace_Scrap_Entry();

        Task<IEnumerable<TraceScrapEntryDTO>> GetTrace_Scrap_Entry(int id);

        void CreateTrace_Scrap_Entry(TraceScrapEntryDTO model);

        void UpdateTrace_Scrap_Entry(TraceScrapEntryDTO model);

        void DeleteTrace_Scrap_Entry(int id);
       
    }
}
