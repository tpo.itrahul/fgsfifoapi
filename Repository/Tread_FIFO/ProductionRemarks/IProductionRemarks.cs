﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Tread_FIFO;
using Entity.TreadFIFO;
namespace Repository.Tread_FIFO
{
    public interface IProductionRemarksRepository : IRepositoryBase<Trace_Production_Remarks>
    {
      Task<IEnumerable<TraceProductionRemarksDTO>> GetTrace_Production_Remarks();

        Task<IEnumerable<TraceProductionRemarksDTO>> GetTrace_Production_Remarks(int id);

       void CreateTrace_Production_Remarks(TraceProductionRemarksDTO model);

      void UpdateTrace_Production_Remarks(TraceProductionRemarksDTO model);

       void DeleteTrace_Production_Remarks(int id);
        

    }
}
