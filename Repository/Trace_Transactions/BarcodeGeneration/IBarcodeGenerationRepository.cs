﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Transactions;
using Entity.TraceTransactions;
namespace Repository.Trace_Transactions
{
    public interface IBarcodeGenerationRepository //: IRepositoryBase<Trace_Barcode_Generation>
    {
         Task<IEnumerable<TraceBarcodeGenerationDTO>> GetTrace_Barcode_Generation();

        Task<IEnumerable<TraceBarcodeGenerationDTO>> GetTrace_Barcode_Generation(int id);

       void CreateTrace_Barcode_Generation(TraceBarcodeGenerationDTO model);

       void UpdateTrace_Barcode_Generation(TraceBarcodeGenerationDTO model);

       void DeleteTrace_Barcode_Generation(int id);
       


    }
}
