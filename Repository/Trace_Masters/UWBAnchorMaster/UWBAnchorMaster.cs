﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class UWBAnchorMasterRepository : RepositoryBase<Trace_UWB_Anchor_Master>, IUWBAnchorMasterRepository
    {
        public UWBAnchorMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceUWBAnchorMasterDTO>> GetTrace_UWB_Anchor_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceUWBAnchorMasterDTO()
            {
                UWB_Anchor_Id = x.UWB_Anchor_Id,
                UWD_Anchor_Name = x.UWD_Anchor_Name,
                UWD_Anchor_Make = x.UWD_Anchor_Make,
                UWD_Anchor_Installed_Date = x.UWD_Anchor_Installed_Date,
                UWD_Anchor_Status = x.UWD_Anchor_Status,

            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceUWBAnchorMasterDTO>> GetTrace_UWB_Anchor_Master(int id)
        {


            var rows = FindByCondition(x => x.UWB_Anchor_Id.Equals(id));
            var results = rows.Select(x => new TraceUWBAnchorMasterDTO()
            {
                UWB_Anchor_Id = x.UWB_Anchor_Id,
                UWD_Anchor_Name = x.UWD_Anchor_Name,
                UWD_Anchor_Make = x.UWD_Anchor_Make,
                UWD_Anchor_Installed_Date = x.UWD_Anchor_Installed_Date,
                UWD_Anchor_Status = x.UWD_Anchor_Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_UWB_Anchor_Master(TraceUWBAnchorMasterDTO model)
        {
            var entity = new Trace_UWB_Anchor_Master();
            entity.UWB_Anchor_Id = model.UWB_Anchor_Id;
            entity.UWD_Anchor_Name = model.UWD_Anchor_Name;
            entity.UWD_Anchor_Make = model.UWD_Anchor_Make;
            entity.UWD_Anchor_Installed_Date = model.UWD_Anchor_Installed_Date;
            entity.UWD_Anchor_Status = model.UWD_Anchor_Status;

            Create(entity);
        }
        public void UpdateTrace_UWB_Anchor_Master(TraceUWBAnchorMasterDTO model)
        {
            var entity = new Trace_UWB_Anchor_Master();
            if (model != null)
            {
                if (model.UWB_Anchor_Id > 0)
                {
                    var data = FindByCondition(x => x.UWB_Anchor_Id == model.UWB_Anchor_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.UWB_Anchor_Id = model.UWB_Anchor_Id;
                entity.UWD_Anchor_Name = model.UWD_Anchor_Name;
                entity.UWD_Anchor_Make = model.UWD_Anchor_Make;
                entity.UWD_Anchor_Installed_Date = model.UWD_Anchor_Installed_Date;
                entity.UWD_Anchor_Status = model.UWD_Anchor_Status;

            }
            Update(entity);
        }
        public void DeleteTrace_UWB_Anchor_Master(int id)
        {
            var entity = new Trace_UWB_Anchor_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.UWB_Anchor_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.UWB_Anchor_Id = 0;
            }

            Update(entity);
        }
    }
}
