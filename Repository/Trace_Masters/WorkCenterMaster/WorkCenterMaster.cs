﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class WorkCenterMasterRepository : RepositoryBase<Trace_WorkCenter_Master>, IWorkCenterMasterRepository
    {
        public WorkCenterMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TraceWorkCenterMasterDTO>> GetTrace_WorkCenter_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TraceWorkCenterMasterDTO()
            {
                WorkCent_Id = x.WorkCent_Id,
                WorkCent_Name = x.WorkCent_Name,
                WorkCent_Type = x.WorkCent_Type,
                Status = x.Status,
                Plant_Id = x.Plant_Id,
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TraceWorkCenterMasterDTO>> GetTrace_WorkCenter_Master(int id)
        {


            var rows = FindByCondition(x => x.WorkCent_Id.Equals(id));
            var results = rows.Select(x => new TraceWorkCenterMasterDTO()
            {
                WorkCent_Id = x.WorkCent_Id,
                WorkCent_Name = x.WorkCent_Name,
                WorkCent_Type = x.WorkCent_Type,
                Status = x.Status,
                Plant_Id = x.Plant_Id,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_WorkCenter_Master(TraceWorkCenterMasterDTO model)
        {
            var entity = new Trace_WorkCenter_Master();
            entity.WorkCent_Id = model.WorkCent_Id;
            entity.WorkCent_Name = model.WorkCent_Name;
            entity.WorkCent_Type = model.WorkCent_Type;
            entity.Status = model.Status;
            entity.Plant_Id = model.Plant_Id;

            Create(entity);
        }
        public void UpdateTrace_WorkCenter_Master(TraceWorkCenterMasterDTO model)
        {
            var entity = new Trace_WorkCenter_Master();
            if (model != null)
            {
                if (model.WorkCent_Id > 0)
                {
                    var data = FindByCondition(x => x.WorkCent_Id == model.WorkCent_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.WorkCent_Id = model.WorkCent_Id;
                entity.WorkCent_Name = model.WorkCent_Name;
                entity.WorkCent_Type = model.WorkCent_Type;
                entity.Status = model.Status;
                entity.Plant_Id = model.Plant_Id;

            }
            Update(entity);
        }
        public void DeleteTrace_WorkCenter_Master(int id)
        {
            var entity = new Trace_WorkCenter_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.WorkCent_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                entity.WorkCent_Id = 0;
            }

            Update(entity);
        }
    }
}
