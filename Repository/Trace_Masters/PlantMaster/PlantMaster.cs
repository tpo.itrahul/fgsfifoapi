﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.Trace_Masters;
using Entity.FGS_FIFO;
using Entity.TraceMasters;
using Microsoft.EntityFrameworkCore;
namespace Repository.Trace_Masters
{
    public class PlantMasterRepository : RepositoryBase<Trace_Plant_Master>, IPlantMasterRepository
    {
        public PlantMasterRepository(Trace_DBContext repositoryContext): base(repositoryContext)
        {
        }
        public async Task<IEnumerable<TracePlantMasterDTO>> GetTrace_Plant_Master()
        {

            var rows = FindAll();
            var results = rows.Select(x => new TracePlantMasterDTO()
            {
                Plant_Id = x.Plant_Id,
                Plant_Name = x.Plant_Name,
                Plant_Address = x.Plant_Address,
                Plant_Status = x.Plant_Status,
         
            }).ToListAsync();

            return await results;

        }
        public async Task<IEnumerable<TracePlantMasterDTO>> GetTrace_Plant_Master(int id)
        {


            var rows = FindByCondition(x => x.Plant_Id.Equals(id));
            var results = rows.Select(x => new TracePlantMasterDTO()
            {
                Plant_Id = x.Plant_Id,
                Plant_Name = x.Plant_Name,
                Plant_Address = x.Plant_Address,
                Plant_Status = x.Plant_Status,
            }).ToListAsync();

            return await results;


        }
        public void CreateTrace_Plant_Master(TracePlantMasterDTO model)
        {
            var entity = new Trace_Plant_Master();

            entity.Plant_Id = model.Plant_Id;
            entity.Plant_Name = model.Plant_Name;
            entity.Plant_Address = model.Plant_Address;
            entity.Plant_Status = model.Plant_Status;

            Create(entity);
        }
        public void UpdateTrace_Plant_Master(TracePlantMasterDTO model)
        {
            var entity = new Trace_Plant_Master();
            if (model != null)
            {
                if (model.Plant_Id > 0)
                {
                    var data = FindByCondition(x => x.Plant_Id == model.Plant_Id);
                    if (data.Any()) { entity = data.FirstOrDefault(); }

                }
                entity.Plant_Id = model.Plant_Id;
                entity.Plant_Name = model.Plant_Name;
                entity.Plant_Address = model.Plant_Address;
                entity.Plant_Status = model.Plant_Status;
             


            }
            Update(entity);
        }
        public void DeleteTrace_Plant_Master(int id)
        {
            var entity = new Trace_Plant_Master();
            if (id != 0)

            {
                var data = FindByCondition(x => x.Plant_Id == id);
                if (data.Any()) { entity = data.FirstOrDefault(); }
                //updated entity status to 0 after finding the invoice id
                entity.Plant_Id = 0;
            }

            Update(entity);
        }
    }
}
