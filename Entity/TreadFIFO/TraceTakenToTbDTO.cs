﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
   public class TraceTakenToTbDTO
    {
        
        public int TakenToTb_Id { get; set; }
    
        public int? Production_Id { get; set; }
        public int? Ageing_Id { get; set; }
        public DateTime? TakenToTb_Date { get; set; }
        public DateTime? TakenToTB_Time { get; set; }
       
        public string? TakenToTB_By { get; set; }
        public int? TakenToTb_Status { get; set; }

    }
}
