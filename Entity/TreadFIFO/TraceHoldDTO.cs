﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TreadFIFO
{
  public  class TraceHoldDTO
    {
       
        public int Hold_Id { get; set; }

      
        public int Production_ID { get; set; }

       
        public int? Hold_Reason_Id { get; set; }
       
        public string? Hold_By { get; set; }
        public DateTime? Hold_Time { get; set; }
        public DateTime? Hold_Date { get; set; }
        public int? Hold_Status { get; set; }
    }
}
