﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
   public class DespatchStatusMasterDTO
    {
       
        public int Desp_Status_Id { get; set; }

        public string Desp_Status_Name { get; set; }
    }
}
