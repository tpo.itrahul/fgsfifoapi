﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.FGS_FIFO;

namespace Entity.FGSFIFO
{
   public class DailyDespatchRequestDTO
    {

       
        public int Desp_Req_Id { get; set; }

        public int Material_Id { get; set; }
          
     public string Material_Desc { get; set; }

       
        public int Customer_id { get; set; }

       
        public int Quantity { get; set; }

       
        public string Requested_DateTime { get; set; }



        public string Entered_By { get; set; }


     
        public string Invoice_No { get; set; }

        public string Invoice_Date { get; set; }

        public int Request_Status { get; set; }

        public ICollection<FGS_FIFO_Daily_Despatch_Transaction> ICFGS_FIFO_Daily_Despatch_Transaction { get; set; }
        
        public string Customer_Name { get; set; }

        public string Available_Locations { get; set; }

        public int Totdespatched { get; set; }
        public string statusName { get; set; }
        public string Material { get; set; }
        public string ReqIdCustMat { get; set; }

        public string Barcode { get; set; }
        public string Locations { get; set; }

        public string ReqNDespacthed { get; set; }
    }
}
