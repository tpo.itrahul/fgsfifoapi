﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Entity.Migrations
{
    public partial class taggentable101tagdtaifk : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "IX_Trace_Tag_Generation_InvTranId",
                table: "Trace_Tag_Generation",
                column: "InvTranId");

            migrationBuilder.AddForeignKey(
                name: "FK_Trace_Tag_Generation_FGS_FIFO_Daily_Receipt_Transaction_InvTranId",
                table: "Trace_Tag_Generation",
                column: "InvTranId",
                principalTable: "FGS_FIFO_Daily_Receipt_Transaction",
                principalColumn: "Inv_TransId",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Trace_Tag_Generation_FGS_FIFO_Daily_Receipt_Transaction_InvTranId",
                table: "Trace_Tag_Generation");

            migrationBuilder.DropIndex(
                name: "IX_Trace_Tag_Generation_InvTranId",
                table: "Trace_Tag_Generation");
        }
    }
}
