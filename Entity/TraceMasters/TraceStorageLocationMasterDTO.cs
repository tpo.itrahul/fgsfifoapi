﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
  public class TraceStorageLocationMasterDTO
    {

       
        public int Storage_Loc_Id { get; set; }

        public string? ShopFloorLocation_Id { get; set; }
        public string? Storage_Loc_Name { get; set; }
    }
}
