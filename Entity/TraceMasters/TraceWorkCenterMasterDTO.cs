﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
   public class TraceWorkCenterMasterDTO
    {
       
        public int WorkCent_Id { get; set; }

        public string? WorkCent_Name { get; set; }
      
        public int Plant_Id { get; set; }
        public string? WorkCent_Type { get; set; }
        public int? Status { get; set; }
    }
}
