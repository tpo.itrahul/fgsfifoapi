﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entity.TraceMasters
{
   public class TraceMHEMasterDTO
    {
      
        public int MHE_Id { get; set; }
      
        public int WorkCent_Id { get; set; }
        public string? MHE_Name { get; set; }
        public string? MHE_Type { get; set; }
        public int? MHE_Status { get; set; }
        public int? Max_Capacity { get; set; }
        public int? Unit_Of_Measurement { get; set; }
    }
}
